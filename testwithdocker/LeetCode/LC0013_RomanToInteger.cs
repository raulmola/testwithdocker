
using System.Collections.Generic;

namespace leetcodes
{
    public class LC0013_RomanToInteger
    {
        public static void Execute()
        {
            var instance = new LC0013_RomanToInteger();

            var input = "IXLM";
            //var input = "III";

            var output = instance.RomanToInt(input);

            Util.Out.Print(input);
            Util.Out.Print(output);
        }
        public int RomanToInt(string s) {
            int l = s.Length;
            s = s + "W"; // sentinel
            int i = 0;
            int curr = 0;
            int next = RToD(s[i]);
            int sum = 0;
            int partialsum = 0;
            do
            {
                partialsum = 0;
                do
                {
                    curr = next;
                    next = RToD(s[i+1]);
                    partialsum += curr;
                    i++;
                }
                while(next > curr);
                sum += (2 * curr - partialsum);
            } while (i < l);

            return sum;
        }

        // Roman To Decimal Compile Time Dictionary as C# specification
        public static int RToD(char roman)
        {
            switch(roman)
            {
                case 'I' : return 1;
                case 'V' : return 5;
                case 'X' : return 10;
                case 'L' : return 50;
                case 'C' : return 100;
                case 'D' : return 500;
                case 'M' : return 1000;
                default  : return 0;
            }

        }
    }
}