namespace leetcodes
{
    public class LC0009_PalindromeNumber
    {
        public static void Execute()
        {
            var instance = new LC0009_PalindromeNumber();
            int x = 123321;
            var output = instance.IsPalindrome(x);
            Util.Out.Print(output);

        }


        public bool IsPalindrome(int x)
        {
            if (x < 0)
                return false;
            int PowerOf10 = 10;
            int leftmostDigits = x;
            int rightmostDigitsReversed = x % 10;
            while (leftmostDigits >= PowerOf10)
            {
                PowerOf10 = PowerOf10 * 10;
                leftmostDigits = leftmostDigits / 10;
                rightmostDigitsReversed = rightmostDigitsReversed * 10 + leftmostDigits % 10;
            }

            if (leftmostDigits < PowerOf10)
                rightmostDigitsReversed = rightmostDigitsReversed / 10;


            return rightmostDigitsReversed == leftmostDigits;
        }

        public bool IsPalindrome_Proofed_WithMaths(int x)
        {
            if (x < 0)
                return false;
            int PowerOf10 = 1;
            int temp = x;
            int reversed = x % 10;
            while (x / PowerOf10 >= 10 * PowerOf10)
            {
                PowerOf10 = PowerOf10 * 10;

                int rest = (x / PowerOf10) % 10;
                reversed = reversed * 10 + rest;
            }



            if ((x / PowerOf10) < PowerOf10)
                reversed = reversed / 10;


            return reversed == (x / PowerOf10);
        }
        public bool IsPalindrome_HalfReversed(int x)
        {
            if (x < 0)
                x = -x;
            int PowerOf10 = 1;
            int temp = x;
            int reversed = 0;
            do
            {
                int rest = temp % 10;
                reversed = reversed * 10 + rest;
                PowerOf10 = PowerOf10 * 10;
                temp = temp / 10;
            }
            while (temp >= PowerOf10);
            if (temp / (PowerOf10 / 10) == 0)
                reversed = reversed / 10;


            return reversed == temp; ;
        }

        public bool IsPalindrome_FullReverseVersion(int x)
        {
            int reversed = 0;
            int temp = x;
            do
            {
                int rest = temp % 10;
                reversed = reversed * 10 + rest;
                temp = temp / 10;
            }
            while (temp > 0);

            return reversed == x;

        }

    }
}