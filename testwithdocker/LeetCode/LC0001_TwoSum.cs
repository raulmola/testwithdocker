namespace leetcodes
{
    public class LC0001_TwoSum
    {
        public static void Execute()
        {
            var instance = new LC0001_TwoSum();
            
            var input = new int[] {2,7,11,15};
            var target = 26;

            var output = instance.TwoSum(input,target);

            Util.Out.Print(input);
            Util.Out.Print(output);
        }

        public int[] TwoSum(int[] input, int target) 
        {
           int i = 0;
           int j = 0;
           int[] output = new int[2] {-1,-1};
           while(i < input.Length - 1)
           {
                j = i + 1;
                while (j < input.Length  )
                {
                    if( input[i]+input[j] == target )
                    {
                         output[0] = i;
                         output[1] = j;

                         return output;
                    }

                    j++;
                }
                i++;
           }

            return output;
        }
    }
}