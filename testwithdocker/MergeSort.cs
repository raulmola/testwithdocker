using System;
namespace leetcodes
{
    public class MergeSort
    {
        private static void MergeSortRecursive(int[] a, int startIndex, int endIndex)
        {
            if (startIndex == endIndex)
            {
            }
            else if (startIndex + 1 == endIndex)
            {
                if (a[endIndex] < a[startIndex])
                    Swap(a, startIndex, endIndex);
            }
            else
            {
                int halfSizedFloor = (endIndex - startIndex + 1) / 2;
                int newStartIndex = startIndex;
                int newHalfIndex = startIndex + halfSizedFloor;
                int newEndIndex = endIndex;
                MergeSortRecursive(a, newStartIndex, newHalfIndex);

                MergeSortRecursive(a, newHalfIndex + 1, newEndIndex);
                MergeSortJoin(a, newStartIndex, newHalfIndex, newEndIndex);
            }

            //		printArray(a);

        }

        private static void printArray(int[] a)
        {
            for (int i = 0; i < a.Length; i++)
            {
                System.Console.Write(a[i]);
                System.Console.Write(",");
            }
            System.Console.WriteLine();
        }

        private static void Swap(int[] a, int i, int j)
        {
            int temp = a[i];
            a[i] = a[j];
            a[j] = temp;
        }
        private static void MergeSortJoin(int[] a, int startIndex, int halfIndex, int endIndex)
        {
            int length = endIndex - startIndex + 1;
            int[] tempChunk = new int[length];

            int leftCursor = startIndex;
            int rightCursor = halfIndex + 1;
            int tempChunkCursor = 0;

            while (leftCursor < halfIndex + 1 && rightCursor < endIndex + 1)
            {
                if (a[leftCursor] < a[rightCursor])
                {
                    tempChunk[tempChunkCursor] = a[leftCursor];
                    leftCursor++;
                }
                else
                {
                    tempChunk[tempChunkCursor] = a[rightCursor];
                    rightCursor++;
                }

                tempChunkCursor++;
            }

            while (leftCursor < halfIndex + 1)
            {
                tempChunk[tempChunkCursor] = a[leftCursor];
                leftCursor++;
                tempChunkCursor++;
            }

            while (rightCursor < endIndex + 1)
            {
                tempChunk[tempChunkCursor] = a[rightCursor];
                rightCursor++;
                tempChunkCursor++;
            }

            tempChunkCursor = 0;
            while (tempChunkCursor < length)
            {
                a[startIndex + tempChunkCursor] = tempChunk[tempChunkCursor];
                tempChunkCursor++;
            }
        }


        public static void MergeSortExample()
        {
            int[] input = new int[] { 9, -1, 8, 7, 6, 5, 8, 4, 3, 3, 100, 3, 3, 3, 3, 3, -2, -1, 2, 1 };

            printArray(input);
            MergeSortRecursive(input, 0, input.Length - 1);
            printArray(input);

        }


    }
}