using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Security.Cryptography.X509Certificates;
using System.Text.Json;

namespace Katas;

//From https://www.codurance.com/es/katas/string-calculator
public class StringCalculator_Kata
{
    private class CustomBracketSeparatorTokensParser
    {
        public static string DefaultSeparatorMark = "//";
        public static string DefaultSeparator = ",";
        public static string DefaultSeparatorOpenBracket = "[";
        public static string DefaultSeparatorCloseBracket = "]";

        private List<string> _tokens;
        private List<string> _separators;
        public IEnumerable<string> Separators { get; private set; }

        public IEnumerable<string> Tokens { get; private set; }

        public CustomBracketSeparatorTokensParser(string input)
        {
            _tokens = new List<string>();
            _separators = new List<string>();

            if (input.StartsWith(DefaultSeparatorMark + DefaultSeparatorOpenBracket))
                while (input.IndexOf(DefaultSeparatorOpenBracket) != -1)
                    input = ExtractOneTokenAndSeparator(input);
            else if (input.StartsWith(DefaultSeparatorMark))
            {
                _tokens.Add(DefaultSeparatorMark);
                _separators.Add(input[(DefaultSeparatorOpenBracket.Length + 1)..(DefaultSeparatorOpenBracket.Length + 2)]);
            }
            else
            {
                _separators.Add(DefaultSeparator);
            }



            Tokens = _tokens;
            Separators = _separators;
        }

        private string ExtractOneTokenAndSeparator(string input)
        {
            var openBracketIndex = input.IndexOf(DefaultSeparatorOpenBracket);
            var closedBracketIndex = input.IndexOf(DefaultSeparatorCloseBracket);

            _tokens.Add(input[..(closedBracketIndex + 1)]);

            var separator = input[(openBracketIndex + 1)..closedBracketIndex];
            if (separator == string.Empty)
                separator = DefaultSeparator;

            _separators.Add(separator);
            return input.Substring(closedBracketIndex + 1);
        }
    }
    public static int Add(string input)
    {
        var separatorTokenParser = new CustomBracketSeparatorTokensParser(input);
        foreach (var token in separatorTokenParser.Tokens.Where(x => x != string.Empty))
            input = input.Replace(token, "");

        string[] splittedOperands = input.Split(separatorTokenParser.Separators.ToArray(), StringSplitOptions.None);

        if (splittedOperands.Length == 1)
            return ParseOperand(splittedOperands[0]);
        else
            return splittedOperands.Select(x => ParseOperand(x)).Sum();
    }

    private static int ParseOperand(string operand)
    {
        int parsedOperand = String.IsNullOrWhiteSpace(operand) ? 0 : int.Parse(operand);

        if (parsedOperand < 0)
            throw new ArgumentException($"Negative values are not allowed, but {parsedOperand} was provided");

        if (parsedOperand >= 1000)
            parsedOperand = 0;

        return parsedOperand;
    }


}