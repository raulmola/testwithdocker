using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.CompilerServices;
using System.Text;

namespace Katas;

public class Bank_Kata
{
	private StringWriter writer;
    private readonly List<AccountLine> accountLines;

	public Bank_Kata(StringWriter writer = null)
    {
        this.writer = writer;
		this.accountLines = new List<AccountLine>();
		accountLines.Add(HeaderAccountLineSingleton.Value);
	}

    public class Transaction
    {
        public Transaction(int amount, DateTime date)
        {
            if (amount <= 0)
                throw new InvalidOperationException("Amount must be greater than zero");

			this.Amount = amount;
			this.Date = date;
		}
		public DateTime Date { get; set; }

        public int Amount { get; set; }

		public override string ToString()
		{
            return string.Format("{0,-10}{1,7:+#;-#;0}", Date.ToString("dd.M.yyyy"), Amount);
		}
	}

	private class DepositTransaction : Transaction
    {
		public DepositTransaction(Transaction transaction) : base(amount: transaction.Amount, date: transaction.Date)
        {
        }

	}
	private class WithDrawTransaction : Transaction
	{
		public WithDrawTransaction(Transaction transaction) : base(amount: transaction.Amount, date: transaction.Date)
		{
			this.Amount = -Amount;
		}
	}

	private class AccountLine
	{
		private readonly Transaction transaction;
		private readonly int balance;
		protected AccountLine()
		{
			balance = 0;
		}

		public AccountLine(Transaction transaction, AccountLine previousLine)
		{
			this.balance = previousLine.balance + transaction.Amount;
			this.transaction = transaction;
		}

		public override string ToString()
		{
			return string.Format("{0}{1,9}", transaction, balance);
		}
	}

	private class HeaderAccountLineSingleton : AccountLine
	{
		private const string Header = "Date        Amount  Balance";
		private static HeaderAccountLineSingleton singleton = new HeaderAccountLineSingleton();
		public static HeaderAccountLineSingleton Value 
		{
			get
			{
				return singleton;
			}
		}
		private HeaderAccountLineSingleton():base()
		{

		}

		public override string ToString()
		{
			return Header;
		}
	}

	public void Deposit(Transaction transaction)
	{
		AddAccountLine(new DepositTransaction(transaction));
	}
	public void Withdraw(Transaction transaction)
    {
		AddAccountLine(new WithDrawTransaction(transaction));
	}

	public void PrintStatement()
    {
		if (accountLines.Last() == HeaderAccountLineSingleton.Value)
			return;

		writer.Write(FormatStatement());
    }

    private string FormatStatement()
    {
		return String.Join(Environment.NewLine, accountLines);
	}

	private void AddAccountLine(Transaction transaction)
	{
		accountLines.Add(new AccountLine(transaction, accountLines.Last()));
	}


}