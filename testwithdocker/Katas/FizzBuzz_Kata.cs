using System;
using System.Runtime.CompilerServices;

namespace Katas;

//From https://www.codurance.com/es/katas/fizzbuzz
public class FizzBuzz_Kata
{
    public static string FizzBuzz(int i)
    {

        // This is what emerged but i know that this is more complicated than needed
        // I could refactor but i won't to understand that this could happen in other katas

        if(i%5 !=0 && i%3 != 0)
            return $"{i}";
        if(i%5 ==0 && i%3 == 0)
            return "FizzBuzz";
        else if(i % 3 == 0)
            return "Fizz";
        else 
            return "Buzz";
            

    }
}