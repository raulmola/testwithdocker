using System.Runtime.Serialization;

namespace Katas;

public class LeapYear_Kata
{

    public static bool IsLeapYear(int year)
    {
        return IsDivisibleBy(year, 400) || !IsDivisibleBy(year, 100) && IsDivisibleBy(year, 4);
    }

    public static bool IsDivisibleBy(int year, int divisor)
    {
        return year % divisor == 0;
    }
}
