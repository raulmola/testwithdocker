using System;

namespace Katas;
public class LC0001_TwoSum_Kata
{
    public LC0001_TwoSum_Kata()
    {
    }

    public int[] TwoSumIndices(int[] nums = null, int target = 0)
    {
        if (nums == null)
            return null;

        if (nums.Length == 0)
            return null;

        if (nums.Length == 1)
            return new int[1] { 0 };

        if (nums.Length >= 2)
        {
            int firstIndex = 0;
            while (firstIndex < nums.Length - 1)
            {
                int secondIndex = CheckIfIndexIsPartOfTheSolution(firstIndex, nums, target);
                if (secondIndex < nums.Length)
                    return new int[2] { firstIndex, secondIndex };

                firstIndex++;
            }

            return null; // This would mean no solution found
        }

        return null;
    }

    private int CheckIfIndexIsPartOfTheSolution(int index, int[] nums, int target)
    {
        int j = index + 1;
        while (j < nums.Length)
        {
            if (nums[index] + nums[j] == target)
                return j;
            j++;
        }

        return j;
    }
}