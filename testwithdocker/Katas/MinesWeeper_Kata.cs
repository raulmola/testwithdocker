using System;
using System.Collections.Generic;
using System.Dynamic;
using System.IO;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Xml;
using System.Xml.Schema;
using Microsoft.VisualBasic;

namespace Katas;
public class MinesWeeper_Kata
{
    public class FieldDimension
    {
        public int Rows { get; }
        public int Columns { get; }

        public FieldDimension(StringReader reader)
        {
            var line = reader.ReadLine();
            string[] dimensions = line.Split(' ');
            Rows = int.Parse(dimensions[0]);
            Columns = int.Parse(dimensions[1]);
        }
    }

    public class FieldIncrementsMatrix
    {
        private const string mine = "*";
        private const string Separator = " ";
        private int[][] _matrix = Array.Empty<int[]>();

        public int this[int a, int b] => _matrix[a][b];
        public int Rows { get; }
        public int Columns { get; }

        public FieldIncrementsMatrix(FieldDimension dimension, StringReader reader)
        {
            Rows = dimension.Rows;
            Columns = dimension.Columns;

            _matrix = new int[Rows][];
            int i = 0;
            string line = line = reader.ReadLine();
            do
            {
                _matrix[i] = line.Split(Separator)
                                 .Select(x => x != mine ? 0 : 1)
                                 .ToArray();
                i++;

                line = reader.ReadLine();
            }
            while (line != null);

        }
    }

    public class NumericFieldBuilder
    {
        private FieldIncrementsMatrix _increments;

        private int[,] _numericField;

        public int Rows { get; }
        public int Columns { get; }



        public int this[int a, int b]
        {
            get
            {
                var count = _increments[a, b];

                if (count == 1)
                    return -1;

                for (int i = -1; i < 2; i++)
                    for (int j = -1; j < 2; j++)
                    {
                        var surroundingCellRow = a + i;
                        var surroundingCellCol = b + j;

                        if (IsSameCell(a, b, surroundingCellRow, surroundingCellCol))
                            continue;

                        if (!IsAValidCell(surroundingCellRow, surroundingCellCol))
                            continue;

                        count += _increments[surroundingCellRow, surroundingCellCol];
                    }

                return count;
            }
        }

        private bool IsSameCell(int a, int b, int surroundingCellRow, int surroundingCellCol)
        {
            return (surroundingCellRow == a && surroundingCellCol == b);
        }

        private bool IsAValidCell(int surroundingCellRow, int surroundingCellCol)
        {
            if (surroundingCellRow < 0 || surroundingCellRow >= _increments.Rows)
                return false;

            if (surroundingCellCol < 0 || surroundingCellCol >= _increments.Columns)
                return false;

            return true;
        }


        public NumericFieldBuilder(FieldIncrementsMatrix increments)
        {
            this._increments = increments;
            this.Rows = _increments.Rows;
            this.Columns = _increments.Columns;

            this._numericField = new int[increments.Rows, increments.Columns];

        }
    }

    public class OutputFieldFormatter
    {
        private NumericFieldBuilder _numericField;

        public OutputFieldFormatter(NumericFieldBuilder numericField)
        {
            this._numericField = numericField;
        }
        public string Format()
        {
            var lines = new string[this._numericField.Rows];

            for (int i = 0; i < _numericField.Rows; i++)
            {
                var characters = Enumerable.Range(0, _numericField.Columns).
                                    Select(j => _numericField[i, j] == -1 ? "*" : _numericField[i, j].ToString());
                lines[i] = String.Join(" ", characters);

            }

            return string.Join("\n", lines);
        }
    }


    // public string ComputeField(string input)
    // {

    //     using (StringReader reader = new StringReader(input))
    //     {
    //         string dimensionLine = reader.ReadLine();

    //         string [] dimensions = dimensionLine.Split(' ');
    //         DimX =  int.Parse(dimensions[0]);
    //         DimY =  int.Parse(dimensions[1]);

    //         string currentLine=reader.ReadLine();
    //         var nextValue = currentLine.Replace(".","0").Replace("* 0","* 1").Replace("0 *","1 *");
    //         string ret = nextValue;
    //         while ((currentLine=reader.ReadLine()) != null)
    //         {
    //             ret+= "\n";
    //             nextValue = currentLine.Replace(".","0").Replace("* 0","* 1").Replace("0 *","1 *");
    //             ret+= nextValue;
    //         }

    //         return ret;
    //     };

    // }

    // public string ComputeField(string input)
    // {
    //    var lines = input.Split("\n");

    //    string [] dimensions =  lines[0].Split(' ');
    //    DimX =  int.Parse(dimensions[0]);
    //    DimY =  int.Parse(dimensions[1]);

    // //    string output ;
    // //    if(lines[1][0] =='*')
    // //     output = "*";
    // //    else
    // //     output = "0";

    //    string output="";
    //    var curLine = lines[1];
    //    for(var i=0 ; i<DimY;i++ )
    //    {
    //         int outputNumber = 0;
    //         string outputChar;
    //         if(curLine[2*i] == '*')
    //             outputChar = "*";
    //         else
    //         {
    //             outputNumber = getCellValue(2*(i-1),curLine) + getCellValue(2*(i+1),curLine);
    //             outputChar = ""+ outputNumber;
    //         }

    //         if(output =="")
    //             output = outputChar;
    //         else 
    //             output = output+ " "+ outputChar;

    //     }

    //    return output;
    // }

    // int getCellValue(int i, string line)
    // {
    //     if ((i<0) || (i>=2 * DimY))
    //         return 0;
    //     else return cellValues[line[i]];
    // }
}