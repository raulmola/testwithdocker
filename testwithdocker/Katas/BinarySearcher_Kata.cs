﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testwithdocker.Katas
{
	public class BinarySearcher
	{
		public const int PositionNotFound = -1;
		private readonly int[]? array;

		public BinarySearcher(int[]? array = null)
		{
			this.array = array;
		}

		public int SearchPosition(int value)
		{
			if (array == null || array.Length == 0)
				return PositionNotFound;

			int arrayStart = 0;
			int arrayEnd = array.Length - 1;

			int l = subArrayLength(arrayStart, arrayEnd);

			while (l > 1)
			{
				int lowerHalf = l / 2;
				int upperHalf = (l + 1) / 2;
				if (value > array[arrayStart + (lowerHalf - 1)])
					arrayStart += lowerHalf;
				else
					arrayEnd -= upperHalf;

				l = subArrayLength(arrayStart, arrayEnd);
			}

			return (value > array[arrayStart]) ? (arrayStart + 1) : arrayStart;
		}
		private int subArrayLength(int start, int end)
		{
			return (end - start) + 1;
		}
	}
}
