﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace testwithdocker.Katas
{
	public class InsertionAlgorithm_Kata
	{
		private readonly int[] array;

		public InsertionAlgorithm_Kata(int[] array = null)
		{
			this.array = array;
		}

		public int SearchInsertionPosition(int value=0)
		{
			if(array == null)
				return -1;

			if (array.Length == 0)
				return 0;

			int i = 2;

			while (i >= 0) 
			{
				if (array.Length > i && value > array[i])
					return i+1;
				i--;
			}

			return 0;
		}
	}
}
