using System;
namespace Util
{
    public static class Out
    {
        public static void Print(int x)
        {
            System.Console.WriteLine(x);

        }

        public static void Print(string x)
        {
            System.Console.WriteLine(x);
        }
        public static void Print(bool x)
        {
            System.Console.WriteLine(x);

        }


        public static void  Print(int[] nums, int limit = -1)
        {
            int i = 0;
            if(limit<0)
                limit = nums.Length;
            else if (limit > nums.Length)
                limit = nums.Length;

            while (i < limit)
            {
                System.Console.Write(nums[i]);
                System.Console.Write(" ");
                i++;
            }
            System.Console.WriteLine();

        }

    }
}