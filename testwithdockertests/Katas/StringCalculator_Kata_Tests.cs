using System.Runtime;
using Katas;
using NuGet.Frameworks;

namespace Katas;


[TestFixture]
public class StringCalculator_Kata_Tests
{
    [Test]
    public void EmptyStringReturnsZero()
    {
        var output = StringCalculator_Kata.Add(String.Empty);

        Assert.That(output, Is.EqualTo(0));
    }

    [TestCase("1", 1)]
    [TestCase("2", 2)]
    [TestCase("0,0", 0)]
    [TestCase("1,2", 3)]
    [TestCase("2,4", 6)]
    [TestCase("2,3,4", 9)]
    [TestCase("1,2,3,4,5,6,7,8,9", 45)]
    [TestCase("1\n,2", 3)]
    [TestCase("\n", 0)]
    [TestCase("\n0\n", 0)]
    [TestCase("\n0,0\n", 0)]
    [TestCase("\n0,\n\n", 0)]
    [TestCase("\n0,\n1\n", 1)]
    [TestCase("\n1\n,\n2\n,\n3\n,\n4\n,\n5\n,\n6\n,\n7\n,\n8\n,\n9", 45)]
    [TestCase("\n\n1\n\n,\n\n2\n\n,\n\n3\n\n,\n\n4\n\n,\n\n5\n\n,\n\n6\n\n,\n\n7\n\n,\n\n8\n\n,\n\n9", 45)]
    [TestCase("//;", 0)]
    [TestCase("//;0", 0)]
    [TestCase("//;0;", 0)]
    [TestCase("//,0,1,", 1)]
    [TestCase("//,\n\n1\n\n,\n\n2\n\n,\n\n3\n\n,\n\n4\n\n,\n\n5\n\n,\n\n6\n\n,\n\n7\n\n,\n\n8\n\n,\n\n9", 45)]
    [TestCase("1000", 0)]
    [TestCase("1000,1001,1002", 0)]
    [TestCase("0,1001,0", 0)]
    [TestCase("//[,]", 0)]
    [TestCase("//[,]1,2", 3)]
    [TestCase("//[,,],,2", 2)]
    [TestCase("//[]2,3", 5)]
    [TestCase("//[***]\n1***2***3", 6)]
    [TestCase("//[][***]\n1***2***3,4***2,3", 15)]
    [TestCase("//[][][][][][***]\n1***2***3,4***2,3", 15)]
    [TestCase("//[*][%]\n1*2%3", 6)]
    [TestCase("//[foo][bar]\n1foo2bar3", 6)]
    public void StringOperatorReturnsExpectedValue(string input, int expected)
    {
        var output = StringCalculator_Kata.Add(input);

        Assert.That(output, Is.EqualTo(expected));
    }

    [TestCase("-1")]
    [TestCase("1,-1")]
    [TestCase("1,2,-3")]
    public void NegativeOperandsThrow(string input)
    {
        Assert.Throws<ArgumentException>(() => StringCalculator_Kata.Add(input));
    }
}

