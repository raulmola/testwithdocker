﻿using testwithdocker.Katas;

namespace testwithdockertests.Katas
{
	[TestFixture]
	public class InsertionAlgorithm_Kata_Tests
	{
		[Test]
		public void NullTest()
		{
			_ = new InsertionAlgorithm_Kata();
		}

		[Test]
		public void CanSendLocatePostion()
		{
			_ = new InsertionAlgorithm_Kata().SearchInsertionPosition();
		}

		[Test]
		public void AcceptsDefaultParameterAtConstructor()
		{
			_ = new InsertionAlgorithm_Kata().SearchInsertionPosition();
		}

		[Test]
		public void CanBeConstructedWithAnEmptyArray()
		{
			var array = Array.Empty<int>();
			_ = new InsertionAlgorithm_Kata(array);
		}

		[Test]
		public void CanConstructWithANonEmptyArray()
		{
			var array = new int[] { 0 };
			_ = new InsertionAlgorithm_Kata(array);
		}

		[Test]
		public void LocatePostionAcceptsDefaultParameter()
		{
			var array = new int[] { 0 };
			var searcher = new InsertionAlgorithm_Kata(array);
			searcher.SearchInsertionPosition(2);
		}

		[Test]
		public void ReturnsMinusIfEmptyArray()
		{
			var array = new int[] { };
			var searcher = new InsertionAlgorithm_Kata(array);

			var position = searcher.SearchInsertionPosition(2);

			Assert.That(position, Is.EqualTo(0));
		}

		[TestCase(0, -1, 0)]
		[TestCase(0, 1, 1)]
		[TestCase(0, 2, 1)]
		[TestCase(0, 3, 1)]
		[TestCase(2, 1, 0)]
		[TestCase(2, 2, 0)]
		[TestCase(2, 1, 0)]
		[TestCase(2, 3, 1)]
		public void ReturnRightPositionWithSingleElementArray(int singleElement, int value, int expected)
		{
			var array = new int[] { singleElement };
			var searcher = new InsertionAlgorithm_Kata(array);

			var position = searcher.SearchInsertionPosition(value);

			Assert.That(position, Is.EqualTo(expected));
		}

		[TestCase(0, 1, 1, 1)]
		[TestCase(0, 1, 2, 2)]
		[TestCase(1, 2, 2, 1)]
		[TestCase(1, 2, 1, 0)]
		[TestCase(1, 2, 3, 2)]
		public void ReturnRightPositionWithTwoSortedElementArray(int a, int b, int value, int expected)
		{
			var array = new int[] { a, b };
			var searcher = new InsertionAlgorithm_Kata(array);

			var position = searcher.SearchInsertionPosition(value);

			Assert.That(position, Is.EqualTo(expected));
		}

		[TestCase(1, 2, 3, 4, 3)]
		public void ReturnRightPositionWithTwoSortedElementArray(int a, int b, int c, int value, int expected)
		{
			var array = new int[] { a, b, c };
			var searcher = new InsertionAlgorithm_Kata(array);

			var position = searcher.SearchInsertionPosition(value);

			Assert.That(position, Is.EqualTo(expected));
		}

	}
}
