
namespace Katas;

[TestFixture]
public class MinesWeeper_Kata_Tests
{

    private static IEnumerable<TestCaseData> ComputeOutputFieldCaseSource
    {
        get
        {

            var input =
        @"1 1
*";
            var expected =
        @"*";

            yield return new TestCaseData(input, expected);

            input =
@"1 1
.";
            expected =
@"0";

            yield return new TestCaseData(input, expected);

            input =
@"1 2
. .";
            expected =
@"0 0";

            yield return new TestCaseData(input, expected);

            input =
@"1 2
* .";
            expected =
@"* 1";

            yield return new TestCaseData(input, expected);


            input =
@"1 2
. *";
            expected =
@"1 *";

            yield return new TestCaseData(input, expected);

            input =
@"1 2
. *";
            expected =
@"1 *";

            yield return new TestCaseData(input, expected);

            input =
@"1 2
* *";
            expected =
@"* *";

            yield return new TestCaseData(input, expected);

            input =
@"2 1
*
*";
            expected =
@"*
*";

            yield return new TestCaseData(input, expected);

            input =
@"2 1
.
.";
            expected =
@"0
0";

            yield return new TestCaseData(input, expected);

            input =
@"4 4
* . . .
. . . .
. * . .
. . . .";

            expected = 
@"* 1 0 0
2 2 1 0
1 * 1 0
1 1 1 0";
            yield return new TestCaseData(input, expected);

            input =
@"3 5
* * . . .
. . . . .
. * . . .";

            expected = 
@"* * 1 0 0
3 3 2 0 0
1 * 1 0 0";
            yield return new TestCaseData(input, expected);

        }
    }

    
    [TestCase("1 1", 1, 1)]
    [TestCase("1 2", 1, 2)]
    [TestCase("1 -2", 1, -2)]
    public void CanParseFieldDimension(string input, int rows, int cols)
    {
        var parser = new MinesWeeper_Kata.FieldDimension(new StringReader(input));

        Assert.That(rows, Is.EqualTo(parser.Rows));
        Assert.That(cols, Is.EqualTo(parser.Columns));
    }

    [TestCase("1 1", "*", 0, 0, 1)]
    [TestCase("1 1", ".", 0, 0, 0)]
    [TestCase("2 1", ".\n.", 1, 0, 0)]
    [TestCase("2 1", ".\n*", 1, 0, 1)]
    [TestCase("3 1", ".\n*\n.", 2, 0, 0)]
    [TestCase("3 1", ".\n*\n*", 2, 0, 1)]
    [TestCase("1 2", ". .", 0, 1, 0)]
    [TestCase("1 2", ". *", 0, 1, 1)]
    [TestCase("2 2", ". .\n. *", 1, 1, 1)]
    [TestCase("2 2", ". .\n. *", 1, 0, 0)]
    [TestCase("2 3", ". . .\n. . *", 1, 2, 1)]

    public void CanParseFieldIncrementsMatrix(string dimension, string field, int a, int b, int expected)
    {
        var fieldDimension = new MinesWeeper_Kata.FieldDimension(new StringReader(dimension));
        var matrix = new MinesWeeper_Kata.FieldIncrementsMatrix(fieldDimension, new StringReader(field));

        Assert.That(matrix[a, b], Is.EqualTo(expected));
    }

    [TestCase("1 1", "*", 0, 0, -1)]
    [TestCase("1 1", ".", 0, 0, 0)]
    [TestCase("1 2", "* *", 0, 0, -1)]
    [TestCase("1 2", "* *", 0, 1, -1)]
    [TestCase("2 1", "*\n*", 0, 0, -1)]
    [TestCase("2 1", "*\n*", 1, 0, -1)]
    [TestCase("1 2", ". *", 0, 0, +1)]
    [TestCase("1 2", "* .", 0, 1, +1)]
    [TestCase("1 3", "* . *", 0, 1, 2)]
    [TestCase("1 3", "* . .", 0, 1, 1)]
    [TestCase("1 3", "* . .", 0, 2, 0)]
    [TestCase("1 3", ". * .", 0, 2, 1)]
    [TestCase("1 3", ". * .", 0, 1, -1)]
    [TestCase("2 1", "*\n*", 0, 0, -1)]
    [TestCase("2 1", "*\n*", 1, 0, -1)]
    [TestCase("2 1", ".\n*", 0, 0, +1)]
    [TestCase("2 1", "*\n.", 1, 0, +1)]
    public void ComputeNumericField(string dimension, string field, int a, int b, int expected)
    {   
        var fieldDimension = new MinesWeeper_Kata.FieldDimension(new StringReader(dimension));
        var matrix = new MinesWeeper_Kata.FieldIncrementsMatrix(fieldDimension, new StringReader(field));

        var numericField = new  MinesWeeper_Kata.NumericFieldBuilder(matrix);

        Assert.That(numericField[a, b], Is.EqualTo(expected));
    }

    [Test,TestCaseSource(nameof(ComputeOutputFieldCaseSource))]
    public void ComputeOutputField(string inputField, string expectedField)
    {   
        var reader = new StringReader(inputField);
        var fieldDimension = new MinesWeeper_Kata.FieldDimension(reader);
        var matrix = new MinesWeeper_Kata.FieldIncrementsMatrix(fieldDimension, reader);

        var numericField = new MinesWeeper_Kata.NumericFieldBuilder(matrix);

        var formater = new  MinesWeeper_Kata.OutputFieldFormatter(numericField);

        var outputField = formater.Format();

        Assert.That(outputField, Is.EqualTo(expectedField));
    }


}