namespace Katas;

public class LeapYear_Kata_Tests
{
    [Test]
    public void OneIsNotLeap()
    {
        var year = 1;

        var isLeapYear  = LeapYear_Kata.IsLeapYear(year);

        Assert.IsFalse(isLeapYear);
    }

    [Test]
    public void TwoIsNotLeap()
    {
        var year = 2;

        var isLeapYear  = LeapYear_Kata.IsLeapYear(year);

        Assert.IsFalse(isLeapYear);
    }

    [Test]
    public void ThreeIsNotLeap()
    {
        var year = 3;

        var isLeapYear  = LeapYear_Kata.IsLeapYear(year);

        Assert.IsFalse(isLeapYear);
    }

    [TestCase(4)]
    [TestCase(8)]
    [TestCase(16)]
    public void DivisibleByFourIsLeap(int year)
    {
        var isLeapYear  = LeapYear_Kata.IsLeapYear(year);

        Assert.IsTrue(isLeapYear);
    }

    [TestCase(400)]
    [TestCase(800)]
    [TestCase(1200)]
    public void DivisibleByFourHundredIsLeap(int year)
    {
        var isLeapYear  = LeapYear_Kata.IsLeapYear(year);

        Assert.IsTrue(isLeapYear);
    }

    [TestCase(500)]
    [TestCase(600)]
    [TestCase(700)]
    public void DivisibleByHundredButNotByFourHundredIsNotLeap(int year)
    {
        var isLeapYear  = LeapYear_Kata.IsLeapYear(year);

        Assert.IsFalse(isLeapYear);
    }

}
