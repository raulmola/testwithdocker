using System.Runtime;
using Katas;
using NuGet.Frameworks;

namespace Katas;


[TestFixture]
public class FizzBuzz_Kata_Tests
{
    [Test]
    public void CanInstantiateFizzBuzzKata()
    {
        new FizzBuzz_Kata();
    }

    [Test]
    public void OneOutputsOne()
    {
        var input = 1;

        var output = FizzBuzz_Kata.FizzBuzz(input);

        Assert.That(output, Is.EqualTo($"{input}"));
    }


    [Test]
    public void ThreeOutputsFizz()
    {
        var input = 3;

        var output = FizzBuzz_Kata.FizzBuzz(input);

        Assert.That(output, Is.EqualTo("Fizz"));
    }

    [Test]
    public void FiveOutputsBuzz()
    {
        var input = 5;

        var output = FizzBuzz_Kata.FizzBuzz(input);

        Assert.That(output, Is.EqualTo("Buzz"));
    }

    [Test]
    public void FifteenOutputsFizzBuzz()
    {
        var input = 15;

        var output = FizzBuzz_Kata.FizzBuzz(input);

        Assert.That(output, Is.EqualTo("FizzBuzz"));
    }

    [TestCase(3)]
    [TestCase(6)]
    [TestCase(9)]
    public void ThreeMultipleReturnsFizz(int input)
    {
        var output = FizzBuzz_Kata.FizzBuzz(input);

        Assert.That(output, Is.EqualTo("Fizz"));
    }

    [TestCase(5)]
    [TestCase(10)]
    [TestCase(20)]
    public void FiveMultipleReturnsBuzz(int input)
    {
        var output = FizzBuzz_Kata.FizzBuzz(input);

        Assert.That(output, Is.EqualTo("Buzz"));
    }

    [TestCase(30)]
    public void FiveAndThreMultipleReturnsFizzBuzz(int input)
    {
        var output = FizzBuzz_Kata.FizzBuzz(input);

        Assert.That(output, Is.EqualTo("FizzBuzz"));
    }

    [TestCase(2)]
    [TestCase(4)]
    public void NorDivisibleByThreeNeitherByFiveReturnsInput(int input)
    {
        var output = FizzBuzz_Kata.FizzBuzz(input);

        Assert.That(output, Is.EqualTo($"{input}"));
    }

}

