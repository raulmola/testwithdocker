
using System.Security.Cryptography;
using System.Text;
using System.Transactions;
using static Katas.Bank_Kata;

namespace Katas;

[TestFixture]
public class Bank_Kata_Tests
{
	[Test]
	public void CanDeposit()
	{
		var deposit = new Bank_Kata.Transaction(amount: 30, date: DateTime.UtcNow);
		new Bank_Kata().Deposit(deposit);
	}

	[Test]
	public void InvalidOperationThrownIfTransactionAnountNonPositive()
	{
		Assert.Throws<InvalidOperationException>(() => new Bank_Kata.Transaction(amount: 0, date: DateTime.UtcNow));
	}

	[Test]
	public void CanWithdraw()
	{
		var transaction = new Bank_Kata.Transaction(30, DateTime.Now);
		new Bank_Kata().Withdraw(transaction);
	}

	[Test]
	public void CanPrintStatement()
	{
		new Bank_Kata().PrintStatement();
	}

	[Test]
	public void BankKataHidesAnStringWriter()
	{
		using (var writer = new StringWriter())
			new Bank_Kata(writer);
	}

	[Test]
	public void WhenNoTransactionPrintedStatementIsEmpty()
	{
		Action<Bank_Kata> action = (bk) =>
		{
			bk.PrintStatement();
		};

		var stringBuilder = ActOnNewBankKata(action);

		Assert.That(stringBuilder.ToString(), Is.EqualTo(string.Empty));
	}

	[Test]
	public void WhenOneTransactionAddedPrintedStatementIsRight()
	{
		string header = @"Date        Amount  Balance";
		string transaction = "24.12.2015   +500      500";
		string expected = String.Concat(header, Environment.NewLine, transaction);

		Action<Bank_Kata> action = (bk) =>
		{ 
			bk.Deposit(new Bank_Kata.Transaction(500, new DateTime(2015, 12, 24)));
			bk.PrintStatement(); 
		};

		var stringBuilder = ActOnNewBankKata(action);

		Assert.That(stringBuilder.ToString(), Is.EqualTo(expected));
	}

	[Test]
	public void WhenTwoTransactionsAddedPrintedStatementIsRight()
	{
		string header = @"Date        Amount  Balance";
		string transaction1 = "24.12.2015   +500      500";
		string transaction2 = "23.8.2016    -100      400";
		string expected = String.Join(Environment.NewLine, new string[] { header, transaction1, transaction2 });

		Action<Bank_Kata> action = (bk) =>
		{
			bk.Deposit(new Bank_Kata.Transaction(500, new DateTime(2015, 12, 24)));
			bk.Withdraw(new Bank_Kata.Transaction(100, new DateTime(2016, 8, 23)));
			bk.PrintStatement();
		};

		var stringBuilder = ActOnNewBankKata(action);

		Assert.That(stringBuilder.ToString(), Is.EqualTo(expected));
	}

	[Test]
	public void WhenThreeTransactionsAddedPrintedStatementIsRight()
	{
		string header = @"Date        Amount  Balance";
		string transaction1 = "24.12.2015   +500      500";
		string transaction2 = "23.8.2016    -100      400";
		string transaction3 = "23.8.2016     -10      390";
		string expected = String.Join(Environment.NewLine, new string[] { header, transaction1, transaction2, transaction3 });

		Action<Bank_Kata> action = (bk) =>
		{
			bk.Deposit(new Bank_Kata.Transaction(500, new DateTime(2015, 12, 24)));
			bk.Withdraw(new Bank_Kata.Transaction(100, new DateTime(2016, 8, 23)));
			bk.Withdraw(new Bank_Kata.Transaction(10, new DateTime(2016, 8, 23)));
			bk.PrintStatement();
		};

		var stringBuilder = ActOnNewBankKata(action);

		Assert.That(stringBuilder.ToString(), Is.EqualTo(expected));
	}


	private StringBuilder ActOnNewBankKata(Action<Bank_Kata> action)
	{
		StringBuilder builder = new StringBuilder();
		Bank_Kata bankKata;
		using (var writer = new StringWriter(builder))
		{
			bankKata = new Bank_Kata(writer);
			action(bankKata);
		}
		return builder;
	}

}