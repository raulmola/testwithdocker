using System.Runtime;
using Katas;
using NuGet.Frameworks;

namespace LC0001_TwoSum_Kata_Tests;


[TestFixture]
public class LC0001_TwoSum_Kata_Tests
{
    [Test]
    public void CanInstantiateTwoSum()
    {
        new LC0001_TwoSum_Kata();
    }

    [Test]
    public void TwoSumAcceptsTwoSumMessage()
    {
        new LC0001_TwoSum_Kata().TwoSumIndices();
    }

    [Test]
    public void TwoSumAcceptsArrayAndTargetParameters()
    {
        new LC0001_TwoSum_Kata().TwoSumIndices(null, 0);
    }

    [Test]
    public void TwoSumReturnsNullIfNumsNull()
    {
        var ret = new LC0001_TwoSum_Kata().TwoSumIndices(null, 0);

        Assert.IsNull(ret);

    }

    [Test]
    public void TwoSumReturnsNullIfNumsEmpty()
    {
        var ret = new LC0001_TwoSum_Kata().TwoSumIndices(new int[0] { }, 0);

        Assert.IsNull(ret);
    }

    //The specifications say that we can assume there exists EXACTLY ONE solution i
    [Test]
    public void TwoSumReturnsZeroIfArrayContainsASingleZero()
    {
        var ret = new LC0001_TwoSum_Kata().TwoSumIndices(new int[1] { 0 }, 0);

        Assert.NotNull(ret);
        Assert.That(ret, Has.Length.EqualTo(1));
        Assert.That(ret.First(), Is.EqualTo(0));

    }

    [Test]
    public void TwoSumReturnsZeroIfNumsArrayContainsASingleElementEqualToTarget()
    {
        AssertReturnsZeroIfNumsArrayContainsASingleElementEqualToTarget(1);
        AssertReturnsZeroIfNumsArrayContainsASingleElementEqualToTarget(2);
        AssertReturnsZeroIfNumsArrayContainsASingleElementEqualToTarget(3);
    }

    [Test]
    public void TwoSumReturnsZeroOneIfNumsArrayContainsTwoElements()
    {
        AssertWithTwoNums(0, 0, 0);
        AssertWithTwoNums(0, 1, 1);
        AssertWithTwoNums(1, 0, 1);
        AssertWithTwoNums(1, 1, 2);
        AssertWithTwoNums(2, 1, 3);
    }

    [Test]
    public void TwoSumReturnsRightIndicesWithThreeNums()
    {
        AssertWithThreeNums(2, 0, 1, 1);
        AssertWithThreeNums(0, 1, 2, 1);
        AssertWithThreeNums(0, 2, 1, 1);
    }

    [Test]
    public void TwoSumReturnsRightIndicesWithFourNums()
    {
        AssertWithFourNums(0, 1, 2, 3, 1);
        AssertWithFourNums(0, 1, 2, 3, 2);
        AssertWithFourNums(0, 1, 2, 3, 3);
        AssertWithFourNums(0, 1, 2, 3, 4);
        AssertWithFourNums(0, 1, 2, 3, 5);
    }
    private void AssertWithFourNums(int num1, int num2, int num3, int num4, int target)
    {
        var nums = new int[4] { num1, num2, num3, num4 };
        var ret = new LC0001_TwoSum_Kata().TwoSumIndices(nums, target);

        Assert.NotNull(ret);
        Assert.That(ret, Has.Length.EqualTo(2));
        var sum = nums[ret[0]] + nums[ret[1]];
        Assert.That(sum, Is.EqualTo(target));

    }
    private void AssertWithThreeNums(int num1, int num2, int num3, int target)
    {
        var nums = new int[3] { num1, num2, num3 };
        var ret = new LC0001_TwoSum_Kata().TwoSumIndices(nums, target);

        Assert.NotNull(ret);
        Assert.That(ret, Has.Length.EqualTo(2));
        var sum = nums[ret[0]] + nums[ret[1]];
        Assert.That(sum, Is.EqualTo(target));

    }
    private void AssertWithTwoNums(int num1, int num2, int target)
    {
        var nums = new int[2] { num1, num2 };
        var ret = new LC0001_TwoSum_Kata().TwoSumIndices(nums, target);

        Assert.NotNull(ret);
        Assert.That(ret, Has.Length.EqualTo(2));
        var sum = nums[ret[0]] + nums[ret[1]];
        Assert.That(sum, Is.EqualTo(target));

    }

    private void AssertReturnsZeroIfNumsArrayContainsASingleElementEqualToTarget(int target)
    {
        var ret = new LC0001_TwoSum_Kata().TwoSumIndices(new int[1] { target }, target);

        Assert.NotNull(ret);
        Assert.That(ret, Has.Length.EqualTo(1));
        Assert.That(ret.First(), Is.EqualTo(0));

    }
}

