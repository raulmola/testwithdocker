﻿using Newtonsoft.Json.Linq;
using System.Runtime.InteropServices;
using testwithdocker.Katas;

namespace testwithdockertests
{
	/// DTSTTCPW , ZOMBIES ,Transformation Priority Premise (TPP), 
	/// 
	/// Problema : Busqueda de la posición de un entero dentro de un array ordenado
	/// 
	/// - Escenario (DONE)	: (Test de ausencia) Carecemos de un objeto con la responsabilidad del problema	///		
	/// - Objetivo			: Crear una clase con nombre apropiado a la que daremos ésa responsabilidad
	/// 
	/// - Escenario (DONE)	: El objeto no encapsula los datos para realizar su responsabilidad 
	///						  (podríamos no necesitar encapsular el array para hacer su funcións)
	/// - Objetivo			: Encapsulación, Information hiding,Cohesión
	///
	/// - Escenario (DONE)	: El objeto responde al mensaje de buscar la posición de un entero dentro de un array
	/// - Objetivo			: Creación de la interfaz de BinarySearcher que responda a su responsabilidad.
	/// 
	/// - Escenario (DONE)	: (ZERO) (Given) El array está vacío (When) Decimos que busque un entero (Then) Devuelve PositionNotFound
	/// - Objetivo			:  Verificar que el objeto responde adecuadamente cuando el array está vacío
	/// 
	/// - Escenario (DONE)	: (ONE) (Given) El array tiene un elemento con valor 1 (When) Pedimos que busque el 1 (Then) Devuelve 0
	/// - Objetivo			:  Verificar que el objeto responde adecuadamente a cierto mensaje
	///
	/// - Escenario (DONE)	: (ONE,BOUNDARY) (Given) El array tiene un elemento con valor 1 (When) Pedimos que busque el 2 (Then) Devuelve 1
	/// - Objetivo			:  Verificar que el objeto responde adecuadamente a cierto mensaje
	/// 
	/// - Escenario (DONE)	: (ONE,BOUNDARY) (Given) El array tiene un elemento con cierto valor V (When) Pedimos que busque donde iría cualquiera menor que el (Then) Devuelve 0
	/// - Objetivo			: Verificar que el objeto responde adecuadamente a cierto mensaje
	/// 
	/// - Escenario (DONE)	: (ONE) (Given) El array tiene un elemento con cierto valor V (When) Pedimos que busque donde iría el valor igual que el V (Then) Devuelve 0
	/// - Objetivo			: Verificar que el objeto responde adecuadamente a cierto mensaje
	/// 
	/// - Escenario (DONE)	: (MANY) (Given) El array tiene dos elementos 0 y 1  (When) Pedimos que buque donde va el -1 (Then) Devuelve 0
	/// - Objetivo			: Verificar que el objeto responde adecuadamente a cierto mensaje
	/// 
	/// - Escenario (DONE)	: (Given) El array tiene tres elementos  (When) Pedimos que buque donde va cierto valor(Then) Busca binariamente su posicion
	/// - Objetivo			: Verificar que el objeto responde adecuadamente a cierto mensaje
	/// 
	/// - Escenario (DONE)	: (Given) El array tiene cuatro elementos  (When) Pedimos que buque donde va cierto valor(Then) Busca binariamente su posicion
	/// - Objetivo			: Verificar que el objeto responde adecuadamente a cierto mensaje
	/// 
	/// - Escenario (DONE)	: (Given) El array tiene cinco elementos  (When) Pedimos que buque donde va cierto valor(Then) Busca binariamente su posicion
	/// - Objetivo			: Verificar que el objeto responde adecuadamente a cierto mensaje

	[TestFixture]
	public class BinarySearch_Kata_Tests
	{
		[Test]
		public void BinarySearcherAnswersMinusOneWhenEmptyArray()
		{
			var searcher = Create();

			int position = searcher.SearchPosition(0);

			Assert.That(position, Is.EqualTo(BinarySearcher.PositionNotFound));

		}

		[Test]
		public void BinarySearcherAnswersSamePositionWhenSingleItem()
		{
			int value = 1;
			var searcher = Create(value);

			int position = searcher.SearchPosition(value);

			Assert.That(position, Is.EqualTo(0));

		}

		[TestCase(1)]
		[TestCase(2)]
		[TestCase(3)]
		public void BinarySearcherWithSingleItemAnswersNextToLastPositionForGreaterValues(int value)
		{
			var searcher = Create(value);

			int position = searcher.SearchPosition(value + 1);

			Assert.That(position, Is.EqualTo(1));
		}

		[TestCase(1)]
		[TestCase(2)]
		[TestCase(3)]
		public void BinarySearcherWithSingleItemAnswersZeroForSmallerValues(int value)
		{
			var searcher = Create(value);

			int position = searcher.SearchPosition(value - 1);

			Assert.That(position, Is.EqualTo(0));
		}

		[TestCase(1)]
		[TestCase(2)]
		[TestCase(3)]
		public void BinarySearcherWithSingleItemAnswersZeroForEqualValues(int value)
		{
			var searcher = Create(value);

			int position = searcher.SearchPosition(value);

			Assert.That(position, Is.EqualTo(0));
		}

		[TestCase(0, 1, -1, 0)]
		[TestCase(0, 1, 0, 0)]
		[TestCase(0, 1, 1, 1)]
		[TestCase(0, 1, 2, 2)]
		[TestCase(0, 2, 1, 1)]
		[TestCase(0, 2, 2, 1)]
		[TestCase(0, 3, 1, 1)]
		[TestCase(0, 3, 0, 0)]
		[TestCase(1, 3, 0, 0)]
		[TestCase(1, 3, 4, 2)]
		public void BinarySearcherWithTwoItemAnswersZeroForEqualValues(int item1, int item2, int value, int expected)
		{
			var searcher = Create(item1, item2);

			int position = searcher.SearchPosition(value);

			Assert.That(position, Is.EqualTo(expected));
		}

		[TestCase(0, 1, 2, -1, 0)]
		[TestCase(0, 1, 2, 0, 0)]
		[TestCase(0, 1, 2, 1, 1)]
		[TestCase(0, 1, 2, 1, 1)]
		public void BinarySearcherWithThreeItemAnswersRightPosition(int item1, int item2, int item3, int value, int expected)
		{
			var searcher = Create(item1, item2, item3);

			int position = searcher.SearchPosition(value);

			Assert.That(position, Is.EqualTo(expected));
		}

		[TestCase(0, 1, 2, 3, -1, 0)]
		[TestCase(0, 1, 2, 3, 0, 0)]
		[TestCase(0, 1, 2, 3, 1, 1)]
		[TestCase(0, 1, 2, 3, 2, 2)]
		[TestCase(0, 1, 2, 3, 3, 3)]
		[TestCase(0, 1, 2, 3, 4, 4)]
		[TestCase(0, 2, 4, 6, 1, 1)]
		[TestCase(0, 2, 4, 6, 2, 1)]
		[TestCase(0, 2, 4, 6, 3, 2)]
		[TestCase(0, 2, 4, 6, 3, 2)]
		[TestCase(0, 2, 4, 6, 4, 2)]
		public void BinarySearcherWithFourItemAnswersRightPosition(int item1, int item2, int item3, int item4, int value, int expected)
		{
			var searcher = Create(item1, item2, item3, item4);

			int position = searcher.SearchPosition(value);

			Assert.That(position, Is.EqualTo(expected));
		}


		[TestCase(0, 1, 2, 3, 4, -1, 0)]
		[TestCase(0, 1, 2, 3, 4, 0, 0)]
		[TestCase(0, 1, 2, 3, 4, 1, 1)]
		[TestCase(0, 1, 2, 3, 4, 1, 1)]
		[TestCase(0, 1, 2, 3, 4, 2, 2)]
		[TestCase(0, 1, 2, 3, 4, 3, 3)]
		[TestCase(0, 1, 2, 3, 4, 4, 4)]
		[TestCase(0, 1, 2, 3, 4, 5, 5)]
		[TestCase(0, 1, 2, 3, 4, 6, 5)]
		[TestCase(0, 2, 4, 6, 8, 1, 1)]
		[TestCase(0, 2, 4, 6, 8, 3, 2)]
		[TestCase(0, 2, 4, 6, 8, 8, 4)]
		[TestCase(0, 2, 4, 6, 8, 9, 5)]
		public void BinarySearcherWithFiveItemAnswersRightPosition(int item1, int item2, int item3, int item4, int item5, int value, int expected)
		{
			var searcher = Create(item1, item2, item3, item4, item5);

			int position = searcher.SearchPosition(value);

			Assert.That(position, Is.EqualTo(expected));
		}


		private BinarySearcher Create(params int[] array)
		{
			return new BinarySearcher(array);
		}

	}
}
